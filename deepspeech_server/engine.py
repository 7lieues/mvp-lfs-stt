"""Module containing speech-to-text transcription functionality"""
import wave
from io import BytesIO

import ffmpeg
import numpy as np
from deepspeech import Model


def normalize_audio(audio):        # convert audio format to .wav with sample_rate= 16000 which is the requires format of mozilla engine
    """Normalize the audio into the format required by DeepSpeech"""

    try:
        out, err = (
        ffmpeg.input("pipe:0")
        .output(
            "pipe:1",
            f="WAV",
            acodec="pcm_s16le",
            ac=1,
            ar="16k",
            loglevel="error",
            hide_banner=None,
        )
        .run(input=audio, capture_stdout=True, capture_stderr=True)
        )
        return ("ok", out)
    except ffmpeg.Error as e:
        print('stdout:', e.stdout.decode('utf8'))
        print('stderr:', e.stderr.decode('utf8'))
        # raise e
        return ("error", e.stderr.decode('utf8'))


class SpeechToTextEngine:        # deepspeech framework use the given models to  perform speech-to-text transcription 
    """Class to perform speech-to-text transcription and related functionality"""
    def __init__(self, model_path, scorer_path):
        self.model = Model(model_path=model_path)
        self.model.enableExternalScorer(scorer_path=scorer_path)

    def run(self, audio):
        """Perform speech-to-text transcription"""
        (status, audio) = normalize_audio(audio)
        if status == "error":
            return audio # we should come back to Umbrella with some meaning (bad audio quality, else)
        audio = BytesIO(audio)
        with wave.Wave_read(audio) as wav:
            audio = np.frombuffer(wav.readframes(wav.getnframes()), np.int16)
        result = self.model.stt(audio_buffer=audio)

        
        return result # we should come back to Umbrella with some sentence (could we have confidence to be stored ?)
