import json, threading, requests, logging, os
from pathlib import Path
from pyhocon import ConfigFactory
from flask import Flask, jsonify, request, json, make_response
from engine import SpeechToTextEngine


logger = logging.getLogger(__name__)
logging.basicConfig(level=10) #logging level in an enviroment variable   #int(os.environ['LOG_LEVEL'])

UMBRELLA_SERVICE = os.environ.get('UMBRELLA_SERVICE')
UMBRELLA_PORT = str(os.environ.get('UMBRELLA_PORT'))
UMBRELLA_ENDPOINT = os.environ.get('UMBRELLA_STT_ENDPOINT')

# Load app configs and initialize DeepSpeech model
conf = ConfigFactory.parse_file("/srv/app/application.conf")

#############   initilise SpeechToTextEngine class with STT model    ##############
engine = SpeechToTextEngine(                            
    model_path=Path(conf["deepspeech.model"]).absolute().as_posix(),
    scorer_path=Path(conf["deepspeech.scorer"]).absolute().as_posix(),
)
app = Flask(__name__)


def convert(audio):

    text = engine.run(audio)         #use rum methode of the initilised objet for the conversion
    transcript_json  =   { "transcript" : text  }    # prepare output json
    logger.debug("##########################################  ")
    logger.debug(   transcript_json   )
    logger.debug(" ########################################## ")
    
    url_postback = "http://" + UMBRELLA_SERVICE + ":" + UMBRELLA_PORT + UMBRELLA_ENDPOINT
    logger.debug(" URL UMBRELLA :  " + url_postback)
    ## POSTING BACK TO UMBRELLA ##
    requests.post(url = url_postback, json = {"message": text})


    ############################         umbrella postback      ################################################
    # # sending post request and saving response as response object
    # r = requests.post(url = "http://" + UMBRELLA_SERVICE + ":4000" + UMBRELLA_ENDPOINT, json = json_body_test)
    # # extracting response text
    # pastebin_url = r.text
    # print("The pastebin URL is:%s"%pastebin_url)
    #############################################################################################################

    return (transcript_json)



@app.route("/ping")
def pong():
    return "pong"


@app.route( conf['server.stt_endpoint'] , methods=['POST'] )
def stt():   
    t1 = threading.Thread(target=convert,args=(request.data,))#create a background thread that will launch the function convert(request.data)
    t1.start()
    logger.debug("***** running STT inference in background *****")
    return "ok"


if __name__ == "__main__":
    app.run(
        host=conf["server.http.host"],
        port=conf["server.http.port"],
        debug=True,
    )
