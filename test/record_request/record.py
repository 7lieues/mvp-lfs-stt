import pyaudio
import wave


wave_output_path = "record_request/audio.wav"

def record_audio(wave_output_path):
	CHUNK = 1024
	FORMAT = pyaudio.paInt16
	CHANNELS = 1
	RATE = 100000
	RECORD_SECONDS = 5

	p = pyaudio.PyAudio()

	stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

	print("* recording")

	frames = [stream.read(CHUNK) for i in range(0, int(RATE / CHUNK * RECORD_SECONDS))]

	print("* done recording")

	stream.stop_stream()
	stream.close()
	p.terminate()

	wf = wave.open(wave_output_path, 'wb')
	wf.setnchannels(CHANNELS)
	wf.setsampwidth(p.get_sample_size(FORMAT))
	wf.setframerate(RATE)
	wf.writeframes(b''.join(frames))
	wf.close()



if __name__ == '__main__':
	record_audio(wave_output_path)
	print("\n RECORDED \n")





# from deepspeech import Model
# import scipy.io.wavfile as wav
# import wave

# import wave
# filename = 'audio/8455-210777-0068.wav'
# w = wave.open(filename, 'r')
# rate = w.getframerate()
# frames = w.getnframes()
# buffer = w.readframes(frames)
# print("rate")
# print(rate)

# print("model.sampleRate()")
# print(model.sampleRate())

# print("##########################")
# type(buffer)
