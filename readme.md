L'unité mvp-tts recoit comme input un clip audio, et retourne son transcript en utilisant la libraire mozilla deepspeesh.
doc officiel de mozilla deepspeesh : https://deepspeech.readthedocs.io/en/latest/
Des contributions des utilisateurs utiles : https://github.com/mozilla/DeepSpeech-examples



Pour faire un pull du reste de repo qui son suivie en LFS :
git lfs fetch --all origin && git lfs pull 


Dans le dossier engine il y a les modèles français de mozilla deepspeesh ; vous trouvez les différent releases si dessous :
https://github.com/Common-Voice/commonvoice-fr/releases
( il faut choisir une version et installé fr-fr.zip et model_tflite_fr.tar.xz ou on trouve les fichiers kenlm.scorer et output_graph.pbmm)

Depuis le dossier test on lance un conteneur test-stt ou on peut enregistrer des clip vocaux et les envoyer en JSON à 7lieues/mvp-tts pour les tests.

Dans le dossier deepspeech_server le fichier app.py met le serveur flask a l'écoute et pointe vers engine.py qui utilise kenlm.scorer et output_graph.pbmm pour la conversion de clip audio en text.
